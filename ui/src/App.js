import React from "react";
import Layout from "./modules/ui/components/Layout";
import { BrowserRouter as Router } from "react-router-dom";

const App = () => {
  return (
    <Router>
      <Layout></Layout>
    </Router>
  );
};

export default App;

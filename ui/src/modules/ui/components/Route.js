import React from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import PlaceList from "../../places/components/PlaceList";
import PlaceDetail from "../../places/components/PlaceDetail";

const UiRoutes = () => {
    return (
        <Routes>
            <Route path="/" element={<Navigate to="/places" />} />
            <Route path="/places/*" element={<PlaceList />} />
            <Route path="/places/:id" element={<PlaceDetail />}/>
        </Routes>
    );
};

export default UiRoutes;
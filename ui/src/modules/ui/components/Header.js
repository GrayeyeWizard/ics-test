import React from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Badge,
  Link,
} from "@mui/material";
import NotificationsIcon from "@mui/icons-material/Notifications";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import logo from "../../../assets/images/icslogo.jfif";
import picture from "../../../assets/images/Piyanut.jpg";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  logo: {
    width: 40,
    height: 40,
    borderRadius: 10,
  },
  spacer: {
    flexGrow: 1,
  },
  picture: {
    width: 40,
    height: 40,
    borderRadius: 40,
    marginLeft: 10,
  },
});

const Header = () => {
  const classes = useStyles();

  return (
    <AppBar position="fixed" sx={{ bgcolor: "#134B8A" }}>
      <Toolbar variant="dense">
        <Link href="/" sx={{ display: { xs: "block", md: "none" } }}>
          <img src={logo} alt="ICS LOGO" className={classes.logo}></img>
        </Link>
        <div className={classes.spacer} />
        <IconButton
          color="inherit"
          sx={{ justifyContent: "center", display: { xs: "none", md: "flex" } }}
        >
          <Badge color="secondary" variant="dot" overlap="circular">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <img src={picture} alt="User" className={classes.picture}></img>
        <Typography
          color="inherit"
          component="div"
          sx={{
            ml: 2,
            fontFamily: "Kanit",
            fontSize: 18,
            display: { xs: "none", md: "block" },
          }}
        >
          Piyanut
        </Typography>
        <IconButton
          color="inherit"
          sx={{
            width: 20,
            height: 20,
            justifyContent: "center",
            display: { xs: "none", md: "flex" },
          }}
        >
          <KeyboardArrowDownIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default Header;

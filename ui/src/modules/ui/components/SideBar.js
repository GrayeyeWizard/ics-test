import React from "react";
import { Box, Link, IconButton, Typography } from "@mui/material";
import logo from "../../../assets/images/icslogo.jfif";
import ArticleOutlinedIcon from "@mui/icons-material/ArticleOutlined";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  logo: {
    width: 40,
    height: 40,
    borderRadius: 10,
    marginTop: 20,
    marginLeft: 8,
  },
});

const SideBar = () => {
  const classes = useStyles();
  return (
    <Box
      sx={{
        width: 60,
        height: 700,
        bgcolor: "#FFF",
        position: "fixed",
        zIndex: "tooltip",
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        boxShadow: 5,
        display: { xs: "none", md: "block" }
      }}
    >
      <Link href="/">
        <img src={logo} alt="ICS LOGO" className={classes.logo}></img>
      </Link>
      <IconButton sx={{display: "block"}}>
        <ArticleOutlinedIcon sx={{bgcolor:"#134B8A", color: "#FFF"}}/>
        <Typography sx={{fontFamily: "Kanit", fontSize:16}}>Place</Typography>
      </IconButton>
    </Box>
  );
};

export default SideBar;

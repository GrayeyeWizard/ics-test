import React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Header from "./Header";
import SideBar from "./SideBar";
import Box from "@mui/material/Box";
import UiRoutes from "./Route";

const Layout = () => {
  return (
    <>
      <CssBaseline></CssBaseline>
      <Header></Header>
      <Box sx={{display: 'flex'}}>
        <SideBar></SideBar>
        <UiRoutes></UiRoutes>
      </Box>
    </>
  );
};

export default Layout;

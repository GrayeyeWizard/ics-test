import React from "react";
import {
  Grid,
  Box,
  Card,
  CardActionArea,
  CardMedia,
  ImageList,
  ImageListItem,
  Typography,
} from "@mui/material";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import { useNavigate } from "react-router-dom";

const PlaceItem = ({
  id,
  images,
  profile_image_url,
  name,
  rating,
  operation_time: { time_open, time_close },
}) => {
  const navigate = useNavigate();
  const selecteItem = () => navigate(`/places/${id}`);

  return (
    <Grid item xs={12} lg={4}>
      <Card onClick={selecteItem} sx={{p: 2, }}>
        <CardActionArea>
          <Box sx={{ display: {sm: "block", md: "flex" }}}>
            <CardMedia
              component="img"
              image={profile_image_url}
              alt={name}
              sx={{ width: {sm: 1, md: 50}, height: {sm: 200, md: 50}, borderRadius: 2 }}
            />
            <Box sx={{ display: "block", width: 1, ml: 1.5 }}>
              <Typography sx={{fontFamily:"Kanit", fontSize: 18, fontWeight: "bold"}}>{name}</Typography>
              <Box sx={{ display: "flex" , justifyContent: "space-between"}}>
                <Box>
                  <CalendarMonthIcon />
                  <Typography>{time_open}</Typography>
                  <Typography>{time_close}</Typography>
                </Box>
                <Typography>{rating}</Typography>
              </Box>
            </Box>
          </Box>
          <ImageList sx={{ width: 1, height: 200, borderRadius: 5, overflow: "hidden"}} cols={3} gap={0}>
            {images.map((item) => (
              <ImageListItem key={item}>
                <img src={item} srcSet={item} alt={name} />
              </ImageListItem>
            ))}
          </ImageList>
        </CardActionArea>
      </Card>
    </Grid>
  );
};

export default PlaceItem;

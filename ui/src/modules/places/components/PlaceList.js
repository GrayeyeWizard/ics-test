import React, { useEffect, useState } from "react";
import axios from "axios";
import { Box, Typography, Grid } from "@mui/material";
import PlaceItem from "./PlaceItem";

const PlaceList = () => {
  const [places, setPlaces] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const { data } = await axios.get("http://localhost:5000/places");
      setPlaces(data);
    };
    getData();
  }, []);

  return (
    <Box sx={{ mt: 8, mr: 5, ml:11 }}>
      <Typography sx={{fontFamily:"Kanit", fontSize: 24, fontWeight:"bold"}}>Place List</Typography>
      <Grid container spacing={2}>
        {places.map((place) => (
          <PlaceItem key={place.id} {...place}></PlaceItem>
        ))}
      </Grid>
    </Box>
  );
};

export default PlaceList;

import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import {
  Box,
  Card,
  CardMedia,
  CardContent,
  Typography,
  ImageList,
  ImageListItem,
} from "@mui/material";

const PlaceDetail = () => {
  const [place, setPlace] = useState([]);
  const [image, setImage] = useState([]);
  const { id } = useParams();

  useEffect(() => {
    const getPlace = async () => {
      const { data } = await axios.get(`http://localhost:5000/places/${id}`);
      setPlace(data);
      setImage(data.images);
    };

    getPlace();
  }, [id]);

  return (
    <Box sx={{ display: {sm:"block", md:"flex"}, mt: 8, mr: 5, ml: 11 }}>
      <Card sx={{ width: {sm: 1, md: 800} }}>
        <CardMedia
          component="img"
          src={place.profile_image_url}
          alt={place.name}
          sx={{ width: 1, height: 400 }}
        ></CardMedia>
        <CardContent>
          <Typography sx={{ display: "flex", justifyContent: "space-between" }}>
            {place.name}
            <Box>{place.rating}</Box>
          </Typography>
          <Typography>{place.address}</Typography>
        </CardContent>
      </Card>
      <Card sx={{ width: {sm: 1, md: 600}, p: 2, ml: 2 }}>
        <Typography>Image</Typography>
        <ImageList
          sx={{ width: 1, height: 600, borderRadius: 5, overflow: "scroll" }}
          rows={3}
          cols={1}
          gap={0}
        >
          {image.map((item) => (
            <ImageListItem key={item} sx={{height:200}}>
              <img src={item} srcSet={item} alt={item} />
            </ImageListItem>
          ))}
        </ImageList>
      </Card>
    </Box>
  );
};

export default PlaceDetail;
